<!DOCTYPE html>
<html>
<head>
    <title>The Weather In The City</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body>
    <div class="container">
        <?php

            require_once 'database.php';
            require_once 'check_city.php';


            if(isset($data))
            {
                $result = $db->query("SELECT id FROM cities WHERE city_name = '$city'");

                $db->query ('SET NAMES utf8');
                $db->query ('SET CHARACTER SET utf8');

                if (!$result->fetch())
                {   
                    $sth = $db->prepare('INSERT INTO `cities`(`city_name`) VALUES (:city)');
                    $sth->bindParam(':city', htmlspecialchars(ucfirst($_GET['city']), ENT_QUOTES, "UTF-8"));
                    $sth->execute();
                    header('location: index.php');
                }
                else
                {
                    echo "The $city is already on the list.<br /> <a href='index.php'>Back</a>";  
                }
            }
         else {
            echo "There are no city such as '$city' in OpenWeatherMap databases.<br /> <a href='index.php'>Back</a>";
        }

        ?>
    </div>
</body>
</html>