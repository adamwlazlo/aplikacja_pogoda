<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>The Weather in The Cities</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="css/style.css" />
    </head>
    <body>
        <div class="container form ">
            <h1>Find out what the weather is in your favorite cities!</h1>
            <div clas="form-group">
                <form enctype="text/plain; charset=utf-8" method="GET" action="add_city.php">
                    <input type="text" name="city" required class="form-control" value=" Enter the city name here..." onClick="this.value=''" >
                    <input type="submit" name="submit" value="Add to list below" class="form-control btn">
                </form>
            </div>

            <?php
                require_once 'database.php';
                $cities_query = $db->query('SELECT city_name FROM cities ORDER BY city_name ASC');
                $cities_list = $cities_query->fetchAll();
            ?>
            <div clas="form-group">
                <form method="GET" action="get_weather.php" id="form" onchange="document.getElementById('form').submit();">
                    <p>Choose a city from the list to see basic weather data</p>
                    <select name="city" size="3" class='form-control'>
                    <?php
                        foreach($cities_list as $city)
                        {
                            echo "<option>{$city['city_name']}</option>";
                        }
                    ?>
                    </select>
                </form>
            </div>
            <div clas="form-group">
                <form method="GET" action="del_city.php" id="form">
                    <p>Choose a city you want remove</p>
                    <select name="city"  class='form-control'>
                    <?php
                        foreach($cities_list as $city)
                        {
                            echo "<option>{$city['city_name']}</option>";
                        }
                    ?>
                    </select>
                    <input type="submit" name="submit" value="Remove from list"  class="form-control btn" >
                </form>
            </<div>
        </div>
    </body>
</html>
