<!DOCTYPE html>
<html>
<head>
    <title>The Weather In The City Of: <?php echo $_GET['city']; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body>

    <?php
    
        require_once 'check_city.php';
        $visibility = $data['visibility']/1000;
        $location_info =  $data['name'].", ".$data['sys']['country'];
        $icon = "<img src='http://openweathermap.org/img/w/".$data['weather'][0]['icon'].".png'>";
        $description = $data['weather'][0]['description'];
    ?>
    <div class="container">
        <div id="top" class="row">
            <p>
                <?php
                    echo $icon;
                    echo " - ".ucfirst($description)." - ";
                    echo "<strong>".$location_info."</strong>";
                    echo ", <a href='index.php'> Back</a>"
                ?>
            </p>
        </div>
        
        <div class="row">
            <div id="basic" col-lg-6 col-sm-12 class='table-responsive-lg'>
                <?php
                    echo "<table class='table table-striped'>
                    <tr><th colspan='2'>Basic current weather data</th></tr>
                    <tr><td>Temperature: </td><td>".$data['main']['temp']."°C</td></tr>
                    <tr><td>Min. Temperature: </td><td>".$data['main']['temp_min']."°C</td></tr>
                    <tr><td>Max. Temperature: </td><td>".$data['main']['temp_max']."°C</td></tr>
                    <tr><td>Pressure: </td><td>".$data['main']['pressure']." hpa</td></tr>
                    <tr><td>Humidity: </td><td>".$data['main']['humidity']."%</td></tr>
                    <tr><td>Visibility: </td><td>".$visibility." km</td></tr>
                    <tr><td>Vind speed: </td><td>".$data['wind']['speed']." m/s</td></tr>
                    <tr><td>Vind direction: </td><td>".$data['wind']['deg']." deg</td></tr>
                    <tr><td>Cloud: </td><td>".$data['clouds']['all']."%</td></tr>
                    <tr><td>Sunrise: </td><td>".date('h:i A', $data['sys']['sunrise'])."</td></tr>
                    <tr><td>Sunset: </td><td>".date('h:i A', $data['sys']['sunset'])."</td></tr>
                    </table>";
                ?>
            </div>

            <div id="fivedays" col-lg-6 col-sm-12 class='table-responsive-md'>
                <?php
                    echo "<table class='table table-striped'>";
                    echo "<caption>Tab. Five day weather forecast</caption>";
                    echo "<tr><th>Weather</th><th>Date</th><th>Temperature [°C]</th><th class='d-none d-sm-block'>Humidity [%]</th><th>Presure [hpa]</th><th class='d-none d-md-block'>Wind speed [m/s]</th><tr>";

                    for($i=0; $i<40; $i+=8)
                    {
                        echo "<tr><td><img src='http://openweathermap.org/img/w/".$data5days['list'][$i]['weather'][0]['icon'].".png'></td>
                              <td>".substr($data5days['list'][$i]['dt_txt'],5,5)."</td>
                              <td>".$data5days['list'][$i]['main']['temp']."</td>
                              <td class='d-none d-sm-block'>".$data5days['list'][$i]['main']['humidity']."</td>
                              <td>".$data5days['list'][$i]['main']['pressure']."</td>
                              
                              <td class='d-none d-md-block'>".$data5days['list'][$i]['wind']['speed']."</td></tr>";
                    }

                    echo "</table>";

                    echo "";
                ?>
            </div>
        </div>
        <div class="row">
            <div col-lg-12>
                <a href='index.php'>Back</a>
            </div>
        </div>
    </div>
</body>
</html>